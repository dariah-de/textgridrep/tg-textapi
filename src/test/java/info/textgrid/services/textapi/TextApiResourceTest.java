package info.textgrid.services.textapi;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.enterprise.context.ApplicationScoped;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.endsWith;

import org.eclipse.microprofile.config.inject.ConfigProperty;


@QuarkusTest
@ApplicationScoped
public class TextApiResourceTest {

    @ConfigProperty(name = "textgrid.host")
    public String textgridHost;

    @ConfigProperty(name = "service.url")
    String serviceUrl;

    @Test
    public void testInfoEndpoint() {
        given()
          .when().get("/textapi/info")
          .then()
             .statusCode(200)
             .body(containsString("tg-textapi"));
    }

    @Test
    public void testManifestEndpointWithSingleObject() {
        given()
          .when().get("/textapi/textgrid:kv2q.0/manifest.json")
          .then()
             .statusCode(200)
             .body("label", is("Alice im Wunderland"))
             .body("id", is(serviceUrl + "/textgrid:kv2q.0/manifest.json"))
             .body("sequence[0].label", is("Alice im Wunderland"))
             ;
    }

    @Test
    public void testManifestEndpointWithAggregation() {
        given()
          .when().get("/textapi/textgrid:wp1j.0/manifest.json")
          .then()
             .statusCode(200)
             .body("label", is("1924"))
             .body("id", is(serviceUrl + "/textgrid:wp1j.0/manifest.json"))
             .body("sequence[0].label", is("Kleine Reise 1923"))
             ;
    }

    @Test
    public void testManifestEndpointFailure() {
        given()
          .when().get("/textapi/textgrid:NOTEXISTING/manifest.json")
          .then()
             .statusCode(500);
    }

    @Test
    public void testItemEndpoint() {
        given()
          .when().get("/textapi/textgrid:kv2q.0/item.json")
          .then()
             .statusCode(200)
             .body("content[0].url", is(textgridHost + "/1.0/aggregator/html/textgrid:kv2q.0?embedded=true"))
             ;
    }

    @Test
    public void testSearchToCollectionPath() {
        given()
          .when().get("/textapi/search/alice/collection.json")
          .then()
             .statusCode(200)
             ;
    }

    @Test
    public void testSearchToCollectionQuery() {
        given()
          .when().get("/textapi/search/collection.json?q=alice")
          .then()
             .statusCode(200)
             ;
    }

    @Test
    public void testIndex() {
        given()
          .when().get("/textapi")
          .then()
             .statusCode(200)
             ;
    }

    @Test
    public void testRedirect() {
        given()
          .when()
          .redirects().follow(false)
          .get("/")
          .then()
             .statusCode(307)
             .header("Location", endsWith("/textapi"))
             ;
    }

}