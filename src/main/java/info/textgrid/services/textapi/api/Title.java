package info.textgrid.services.textapi.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "title",
  "type",
})
public class Title {
 
    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/title.jsonld";
    public String title;
    public String type = "main";

    public Title setTitle(String title) {
        this.title = title;
        return this;
    }

    public Title setType(String type) {
        this.type = type;
        return this;
    }
    
}
