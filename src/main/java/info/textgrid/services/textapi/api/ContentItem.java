package info.textgrid.services.textapi.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "url",
  "type",
})
public class ContentItem {
    
    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/content.jsonld";
    public String type = "text/html;type=transcription";
    public String url;

    public ContentItem setUrl(String url) {
        this.url = url;
        return this;
    }


}
