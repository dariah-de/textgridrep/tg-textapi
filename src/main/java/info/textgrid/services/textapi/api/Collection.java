package info.textgrid.services.textapi.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "textapi",
  "id",
  "title",
  "collector",
  "description",
  "sequence",
})
public class Collection {

    public final String textapi = "1.1.0";

    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/collection.jsonld";
    public String id;
    public List<Title> title = new ArrayList<Title>();
    public List<Actor> collector = new ArrayList<Actor>();
    public String description;
    public List<SequenceItem> sequence = new ArrayList<SequenceItem>();

    public Collection setId(String id) {
        this.id = id;
        return this;
    }

    public Collection addTitle(Title title) {
        this.title.add(title);
        return this;
    }

    public Collection addCollector(Actor collector) {
        this.collector.add(collector);
        return this;
    }

    public Collection setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<SequenceItem> getSequence() {
        return sequence;
    }

    public Collection setSequence(List<SequenceItem> sequence) {
        this.sequence = sequence;
        return this;
    }

    public Collection addSequenceItem(SequenceItem item) {
        this.sequence.add(item);
        return this;
    }
}
