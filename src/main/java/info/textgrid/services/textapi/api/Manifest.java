package info.textgrid.services.textapi.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "textapi",
  "id",
  "label",
  "repository",
  "license",
  "sequence",
})
public class Manifest {
    
    public final String textapi = "1.1.0";

    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/manifest.jsonld";

    public String id;
    public String label;

    public List<Repository> repository = new ArrayList<Repository>();
    public List<License> license = new ArrayList<License>();
    public List<SequenceItem> sequence = new ArrayList<SequenceItem>();

    public String getTextapi() {
        return textapi;
    }

    public String getId() {
        return id;
    }

    public Manifest setId(String id) {
        this.id = id;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public Manifest setLabel(String label) {
        this.label = label;
        return this;
    }

    public List<Repository> getRepository() {
        return repository;
    }

    public Manifest setRepository(List<Repository> repository) {
        this.repository = repository;
        return this;
    }

    public Manifest addRepository(Repository repository) {
        this.repository.add(repository);
        return this;
    }

    public List<License> getLicense() {
        return license;
    }

    public Manifest setLicense(List<License> license) {
        this.license = license;
        return this;
    }

    public Manifest addLicense(License license) {
        this.license.add(license);
        return this;
    }

    public List<SequenceItem> getSequence() {
        return sequence;
    }

    public Manifest setSequence(List<SequenceItem> sequence) {
        this.sequence = sequence;
        return this;
    }

    public Manifest addSequenceItem(SequenceItem item) {
        this.sequence.add(item);
        return this;
    }


}
