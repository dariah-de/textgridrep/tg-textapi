package info.textgrid.services.textapi.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "role",
  "name",
  "id",
})
public class Actor {

    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/actor.jsonld";
    public List<String> role = new ArrayList<String>();
    public String name;
    public String id;

    public Actor addRole(String role) {
        this.role.add(role);
        return this;
    }

    public Actor setName(String name) {
        this.name = name;
        return this;
    }
    
    public Actor setId(String id) {
        this.id = id;
        return this;
    }

}
