package info.textgrid.services.textapi.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "id",
})
public class License {

    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/license.jsonld";
    public String id;

    public License setId(String id) {
        this.id = id;
        return this;
    }

}
