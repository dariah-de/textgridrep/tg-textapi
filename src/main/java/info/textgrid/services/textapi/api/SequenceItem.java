package info.textgrid.services.textapi.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "id",
  "type",
  "label",
})
public class SequenceItem {
    
    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/sequence.jsonld";
    public String id;
    public String type = "item";
    public String label;

    public SequenceItem setId(String id) {
        this.id = id;
        return this;
    }

    public SequenceItem setType(String type) {
        this.type = type;
        return this;
    }

    public SequenceItem setLabel(String label) {
        this.label = label;
        return this;
    }    

}
