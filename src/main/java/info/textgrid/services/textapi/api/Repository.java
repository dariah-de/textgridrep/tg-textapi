package info.textgrid.services.textapi.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "label",
  "url",
  "baseUrl",
  "id",
})
public class Repository {

    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/repository.jsonld";

    public String baseUrl = "https://textgridrep.org/";
    public String id;
    public String label = "TextGrid Repository";
    public String url = "https://textgrid.de/";

    public Repository setId(String id) {
        this.id = id;
        return this;
    }
}
