package info.textgrid.services.textapi.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
  "@context",
  "textapi",
  "id",
  "type",
  "n",
  "lang",
  "content",
})
public class Item {

    public final String textapi = "1.1.0";

    @JsonProperty("@context")
    public final String _context = "https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/item.jsonld";

    public String id = "";

    public String type = "section";
    public String n = "Cheat";
    public List<String> lang = new ArrayList<String>();

    public List<ContentItem> content = new ArrayList<ContentItem>();

    public Item setContent(List<ContentItem> content) {
        this.content = content;
        return this;
    }

    public Item addContent(ContentItem item) {
        this.content.add(item);
        return this;
    }

    public Item addLang(String lang) {
        this.lang.add(lang);
        return this;
    }

    public Item setId(String id) {
        this.id = id;
        return this;
    }
    
}
