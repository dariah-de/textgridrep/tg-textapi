package info.textgrid.services.textapi.services;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import info.textgrid.clients.SearchClient;
import jakarta.inject.Singleton;

@Singleton
public class SearchClientService extends SearchClient {

    public SearchClientService(@ConfigProperty(name = "textgrid.search.url") String endpoint) {
        super(endpoint);
    }
    
}
