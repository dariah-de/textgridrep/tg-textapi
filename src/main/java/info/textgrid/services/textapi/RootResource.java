package info.textgrid.services.textapi;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.microprofile.openapi.annotations.Operation;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/")
public class RootResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Operation(hidden = true) // hide operation from OpenAPI
    public Response rootindex() throws URISyntaxException {
        return Response.temporaryRedirect(new URI("/textapi")).build();
    }

}
