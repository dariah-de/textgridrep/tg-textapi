package info.textgrid.services.textapi;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.Operation;

import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.portal.Project;
import info.textgrid.services.textapi.services.SearchClientService;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/textapi/project")
public class ProjectResource {

    @Inject
    SearchClientService searchClient;

    @ConfigProperty(name = "textgrid.host")
    String textgridHost;

    @ConfigProperty(name = "service.url")
    String serviceUrl;

    @Inject
    Template project;

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/{projectId}")
    @Operation(hidden = true) // hide operation from OpenAPI
    public TemplateInstance index(String projectId) {

        Project projectConf = searchClient.projectQuery().projectDetails(projectId);
        Response res = searchClient.navigationQuery().listProject(projectId);

        return project
            .data("projectId", projectId)
            .data("serviceUrl", serviceUrl)
            .data("projectConf", projectConf)
            .data("textgridHost", textgridHost)
            .data("res", res);

    }



}
