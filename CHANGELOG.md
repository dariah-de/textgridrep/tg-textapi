
<a name="0.1.35"></a>
## [0.1.35](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-textapi/compare/0.1.34...0.1.35) (2024-07-10)

### Bug Fixes

* (workaround) service is reachable at path /textapi online


<a name="0.1.34"></a>
## [0.1.34](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-textapi/compare/0.1.33...0.1.34) (2024-07-08)

### Features

* example links to v4 version of tido


<a name="0.1.33"></a>
## [0.1.33](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-textapi/compare/0.1.32...0.1.33) (2024-07-08)

### Features

* example for a project specific textapi object listing


<a name="0.1.32"></a>
## [0.1.32](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-textapi/compare/0.1.31...0.1.32) (2024-06-07)


<a name="0.1.31"></a>
## [0.1.31](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-textapi/compare/0.1.30...0.1.31) (2024-06-06)


<a name="0.1.30"></a>
## [0.1.30](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-textapi/compare/0.1.29...0.1.30) (2024-06-06)


<a name="0.1.29"></a>
## [0.1.29](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-textapi/compare/0.1.28...0.1.29) (2024-06-06)


<a name="0.1.28"></a>
## [0.1.28](https://gitlab.gwdg.de/dariah-de/textgridrep/tg-textapi/compare/0.1.27...0.1.28) (2023-12-20)

### Bug Fixes

* **k8s:** deployment resouces in helmchart shall be idempotent


<a name="0.1.27"></a>
## 0.1.27 (2023-12-20)

